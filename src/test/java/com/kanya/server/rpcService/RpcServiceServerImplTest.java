package com.kanya.server.rpcService;

import com.kanya.server.dao.PatientDao;
import com.kanya.shared.Category;
import com.kanya.shared.Patient;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RpcServiceServerImplTest {

    private static final int PATIENT_TO_TEST_ID_1 = 1;
    private static final int PATIENT_TO_TEST_ID_2 = 2;
    private static final int PATIENT_TO_TEST_ID_3 = 3;

    private static final int SUCCESSFUL_RESULT = 1;
    private static final int NUMBER_OF_PATIENTS_TO_CREATE = 11;

    @InjectMocks
    RpcServiceServerImpl rpcServiceServerImpl;

    @Mock
    PatientDao patientDao;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void addPatient() {

        Patient patientToTest = createPatientToTest(PATIENT_TO_TEST_ID_1);

        Mockito.doNothing().when(patientDao).savePatient(any(Patient.class));

        int result = rpcServiceServerImpl.addPatient(patientToTest);

        Assert.assertEquals(SUCCESSFUL_RESULT, result);
        verify(patientDao).savePatient(any(Patient.class));
    }

    @Test
    public void getPatientById() {

        Patient patientExpected = createPatientToTest(PATIENT_TO_TEST_ID_1);
        when(patientDao.getPatient(PATIENT_TO_TEST_ID_1)).thenReturn(patientExpected);

        Patient patientActual = rpcServiceServerImpl.getPatientById(PATIENT_TO_TEST_ID_1);

        Assert.assertEquals(patientExpected, patientActual);
        verify(patientDao).getPatient(PATIENT_TO_TEST_ID_1);
    }

    @Test
    public void getListOfPatients() {
        Patient patient1 = createPatientToTest(PATIENT_TO_TEST_ID_1);
        Patient patient2 = createPatientToTest(PATIENT_TO_TEST_ID_2);
        Patient patient3 = createPatientToTest(PATIENT_TO_TEST_ID_3);
        List<Patient> expectedListOfPatients = new ArrayList<>();
        expectedListOfPatients.add(patient1);
        expectedListOfPatients.add(patient2);
        expectedListOfPatients.add(patient3);

        when(patientDao.getAllPatientsFromDb()).thenReturn(expectedListOfPatients);

        List<Patient> actualdListOfPatients = rpcServiceServerImpl.getListOfPatients();

        Assert.assertEquals(expectedListOfPatients, actualdListOfPatients);
        verify(patientDao).getAllPatientsFromDb();
    }

    @Test
    public void deletePatient() {

    }

    @Test
    public void editPatient() {

        Patient patientToTest = createPatientToTest(PATIENT_TO_TEST_ID_1);

        Mockito.doNothing().when(patientDao).updatePatientInDb(any(Patient.class));

        int result = rpcServiceServerImpl.editPatient(patientToTest);

        Assert.assertEquals(SUCCESSFUL_RESULT, result);
        verify(patientDao).updatePatientInDb(any(Patient.class));
    }

    @Test
    public void fillOutDbWithRandomData() {

        Mockito.doNothing().when(patientDao).fillOutDbWithRandomData(anyInt());

        int result = rpcServiceServerImpl.fillOutDbWithRandomData(NUMBER_OF_PATIENTS_TO_CREATE);

        Assert.assertEquals(SUCCESSFUL_RESULT, result);
        verify(patientDao).fillOutDbWithRandomData(anyInt());
    }

    private Patient createPatientToTest(int patientId) {
        Patient patientToTest = new Patient();
        patientToTest.setId(patientId);
        patientToTest.setFirstName("Andrew");
        patientToTest.setLastName("Clark");
        patientToTest.setAddress("22 New Street");
        patientToTest.setCategory(Category.FUNNY);
        return patientToTest;
    }
}