package com.kanya.server.dao;

import com.kanya.server.Constrants;
import com.kanya.server.HibernateUtil;
import org.apache.commons.lang.NotImplementedException;
import org.dbunit.DBTestCase;
import org.dbunit.PropertiesBasedJdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.Before;

import java.util.logging.Level;
import java.util.logging.Logger;


public abstract class HibernateDbUnitAbstractTestCase extends DBTestCase {

    private static final java.util.logging.Logger LOGGER = Logger.getLogger(HibernateDbUnitAbstractTestCase.class.toString());
    protected SessionFactory sessionFactory;
    protected Session session;

    public HibernateDbUnitAbstractTestCase() {

        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_DRIVER_CLASS, Constrants.getDatabaseDriverClass());
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_CONNECTION_URL, Constrants.getDatabaseUrl());
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_USERNAME, Constrants.getDatabaseUsername());
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_PASSWORD, Constrants.getDatabasePassword());
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_SCHEMA, Constrants.getDatabaseTestSchema());

    }

    @Before
    public void setUp() throws Exception {
        super.setUp();
        LOGGER.log(Level.INFO, "setUp run");
        HibernateUtil.buildTestSessionFactory();
        sessionFactory = HibernateUtil.getSessionFactory();
        LOGGER.log(Level.INFO, "sessionFactory is null" + (sessionFactory == null) );
        session = HibernateUtil.getSession();

    }

    @After
    public void tearDown() throws Exception {
        session.close();
        super.tearDown();
    }

    /**
     * {@inheritDoc}
     */
    protected IDataSet getDataSet() throws Exception {
        throw new NotImplementedException("Specify data set for test: " + this.getClass().getSimpleName());
    }

    /**
     * {@inheritDoc}
     */
    protected DatabaseOperation getSetUpOperation() throws Exception {
        return DatabaseOperation.REFRESH;
    }

    /**
     * {@inheritDoc}
     */
    protected DatabaseOperation getTearDownOperation() throws Exception {
        return DatabaseOperation.DELETE;
    }

    public SessionFactory getSessionFactory() {
        LOGGER.log(Level.INFO, "sessionFactory is null = " + (sessionFactory == null) );
        return sessionFactory;
    }
}