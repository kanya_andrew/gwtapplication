package com.kanya.server.dao;

import com.kanya.server.dao.Impl.PatientDaoImpl;
import com.kanya.shared.Category;
import com.kanya.shared.Patient;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.hibernate.query.Query;
import org.junit.Assert;
import org.junit.Test;

import java.io.FileInputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PatientDaoTest extends HibernateDbUnitAbstractTestCase {
    private static final java.util.logging.Logger LOGGER = Logger.getLogger(HibernateDbUnitAbstractTestCase.class.toString());
    private static final int PATIENT_TO_TEST_ID_1 = 1;
    private static final int NUMBER_OF_PATIENTS_TO_GENERATE = 13;

    private PatientDao patientDao;

    @Test
    public void testSavePatient() {
        Patient patientToSave = createPatientToTest(PATIENT_TO_TEST_ID_1);
        Patient expectedPatient;
        Patient actualPatient;
        patientDao = new PatientDaoImpl(getSessionFactory());

        LOGGER.log(Level.INFO, "patientToSave = " + patientToSave);
        patientDao.savePatient(patientToSave);

        String hql = "select max(id) from patient";
        List list = session.createQuery(hql).list();
        session.beginTransaction();

        int maxId = ((Integer) list.get(0)).intValue();
        LOGGER.log(Level.INFO, "maxId = " + maxId);

        actualPatient = session.get(Patient.class, maxId);
        LOGGER.log(Level.INFO, "actualPatient = " + actualPatient);

        expectedPatient = patientToSave;
        expectedPatient.setId(maxId);
        LOGGER.log(Level.INFO, "expectedPatient = " + actualPatient);

        session.delete(actualPatient);
        session.getTransaction().commit();

        Assert.assertEquals(expectedPatient, actualPatient);
    }

    @Test
    public void testUpdatePatientInDb() {
        patientDao = new PatientDaoImpl(getSessionFactory());
        Patient patientToUpdate = session.get(Patient.class, PATIENT_TO_TEST_ID_1);
        patientToUpdate.setFirstName("Elan");
        LOGGER.log(Level.INFO, "patientToUpdate = " + patientToUpdate);


        patientDao.updatePatientInDb(patientToUpdate);

        Patient patientAfterUpdate = session.get(Patient.class, PATIENT_TO_TEST_ID_1);
        LOGGER.log(Level.INFO, "patientAfterUpdate = " + patientAfterUpdate);

        Assert.assertEquals(patientToUpdate, patientAfterUpdate);
    }

    @Test
    public void testGetAllPatientsFromDb() {
        patientDao = new PatientDaoImpl(getSessionFactory());

        session.beginTransaction();

        Query query = session.createQuery(" from patient order by id");
        int expectedNumberOfPatients = query.list().size();

        session.getTransaction().commit();

        List<Patient> patientList = patientDao.getAllPatientsFromDb();

        int actualNumberOfPatients = patientList.size();
        LOGGER.log(Level.INFO, "actualValue = " + actualNumberOfPatients);
        LOGGER.log(Level.INFO, "expectedValue = " + expectedNumberOfPatients);

        Assert.assertEquals(actualNumberOfPatients, expectedNumberOfPatients);
    }

    @Test
    public void testFillOutDbWithRandomData() {
        patientDao = new PatientDaoImpl(getSessionFactory());
        Query query = session.createQuery(" from patient order by id");

        int numberOfPatientsBeforeGeneration = query.list().size();
        patientDao.fillOutDbWithRandomData(NUMBER_OF_PATIENTS_TO_GENERATE);

        int numberOfPatientsAfterGeneration = patientDao.getAllPatientsFromDb().size();

        session.beginTransaction();

        String hql = "select max(id) from patient";
        List list = session.createQuery(hql).list();
        int maxId = ((Integer) list.get(0)).intValue();

        for (int i = 0; i < NUMBER_OF_PATIENTS_TO_GENERATE; i++) {
            session.delete(session.get(Patient.class, maxId));
            maxId--;

        }
        session.getTransaction().commit();

        Assert.assertEquals(numberOfPatientsBeforeGeneration + NUMBER_OF_PATIENTS_TO_GENERATE, numberOfPatientsAfterGeneration);
    }

    @Test
    public void testGetPatient() {
        patientDao = new PatientDaoImpl(getSessionFactory());
        Patient expectedPatient = session.get(Patient.class, PATIENT_TO_TEST_ID_1);
        LOGGER.log(Level.INFO, "expectedPatient = " + expectedPatient);

        Patient actualPatient = patientDao.getPatient(PATIENT_TO_TEST_ID_1);
        LOGGER.log(Level.INFO, "actualPatient = " + actualPatient);

        Assert.assertEquals(expectedPatient, actualPatient);
    }

    private Patient createPatientToTest(int patientId) {
        Patient patientToTest = new Patient();
        patientToTest.setId(patientId);
        patientToTest.setFirstName("Adam");
        patientToTest.setLastName("Smith");
        patientToTest.setAddress("33 New Street");
        patientToTest.setCategory(Category.NEW);
        patientToTest.setDeleted(false);
        return patientToTest;
    }

    protected IDataSet getDataSet() throws Exception {
        return new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/patientDataSet.xml"));
    }
}