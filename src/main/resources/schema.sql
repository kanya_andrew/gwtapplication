-- -----------------------------------------------------
-- Schema gwt_example
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS gwt_example;

-- -----------------------------------------------------
-- Table `gwt_example`.`patientToEdit`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS gwt_example.patientToEdit (
  id                SERIAL       NOT NULL,
  first_name        VARCHAR(255) NOT NULL,
  last_name         VARCHAR(255) NOT NULL,
  address           VARCHAR(255) NOT NULL,
  category_id       INT          NOT NULL,
  deleted           BOOLEAN      NOT NULL DEFAULT FALSE,
  PRIMARY KEY (id)
);
