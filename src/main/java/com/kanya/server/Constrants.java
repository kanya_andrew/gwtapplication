package com.kanya.server;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Constrants {

    private static Properties prop = new Properties();
    private static InputStream input = null;

    private static final String CURRENT_DB = "current_db.properties";

    static {

        ClassLoader loader = Thread.currentThread().getContextClassLoader();

        try {

            input = loader.getResourceAsStream(CURRENT_DB);
            // load a properties file
            prop.load(input);
            String currentDb = prop.getProperty("currentDbConfig");

            input = loader.getResourceAsStream(currentDb);
            // load a properties file
            prop.load(input);

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static String getDatabaseDriverClass() {
        return prop.getProperty("driverClass");
    }

    public static String getDatabaseUsername() {
        return prop.getProperty("username");
    }

    public static String getDatabasePassword() {
        return prop.getProperty("password");
    }

    public static String getDatabaseUrl() {
        return prop.getProperty("url");
    }

    public static String getDatabaseSchema() {
        return prop.getProperty("schema");
    }

    public static String getDatabaseTestSchema() {
        return prop.getProperty("testSchema");
    }

}
