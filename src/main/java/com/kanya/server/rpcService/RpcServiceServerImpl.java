package com.kanya.server.rpcService;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.kanya.client.rpcService.RpcService;
import com.kanya.server.HibernateUtil;
import com.kanya.server.dao.Impl.PatientDaoImpl;
import com.kanya.server.dao.PatientDao;
import com.kanya.shared.Patient;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RpcServiceServerImpl extends RemoteServiceServlet implements RpcService {

    private static final Logger LOGGER = Logger.getLogger(RpcServiceServerImpl.class.toString());

    PatientDao patientDao = new PatientDaoImpl(HibernateUtil.getSessionFactory());

    @Override
    public int addPatient(Patient patient) {
        LOGGER.log(Level.INFO, "addPatient() started");
        LOGGER.log(Level.INFO, "addPatient() got: " + patient.toString());
        patientDao.savePatient(patient);
        return 1;
    }

    @Override
    public Patient getPatientById(int patientId) {
        return patientDao.getPatient(patientId);
    }

    @Override
    public List<Patient> getListOfPatients() {
        LOGGER.log(Level.INFO, "getListOfPatients() started");
        List<Patient> patientList = patientDao.getAllPatientsFromDb();
        return patientList;
    }

    @Override
    public int deletePatient(Patient patient) {
        return 0;
    }

    @Override
    public int editPatient(Patient patient) {
        patientDao.updatePatientInDb(patient);
        return 1;
    }

    @Override
    public int fillOutDbWithRandomData(int numberOfPatients) {
        patientDao.fillOutDbWithRandomData(numberOfPatients);
        return 1;
    }


}
