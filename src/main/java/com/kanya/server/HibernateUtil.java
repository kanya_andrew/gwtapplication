package com.kanya.server;

import com.kanya.shared.Category;
import com.kanya.shared.Patient;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;


public class HibernateUtil {
    private static SessionFactory sessionFactory;
    private static Configuration mainConfiguration = new Configuration();

    public static void buildSessionFactory() {
        try {
            mainConfiguration.configure();
            mainConfiguration.setProperty("hibernate.connection.username", Constrants.getDatabaseUsername());
            mainConfiguration.setProperty("hibernate.connection.password", Constrants.getDatabasePassword());
            mainConfiguration.setProperty("hibernate.connection.url", Constrants.getDatabaseUrl() + "?currentSchema=" + Constrants.getDatabaseSchema());
            mainConfiguration.addAnnotatedClass(Patient.class);
            mainConfiguration.addAnnotatedClass(Category.class);
            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(mainConfiguration.getProperties()).build();

            sessionFactory = mainConfiguration.buildSessionFactory(serviceRegistry);
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static void buildTestSessionFactory() {
        try {
            mainConfiguration.configure();
            mainConfiguration.setProperty("hibernate.connection.username", Constrants.getDatabaseUsername());
            mainConfiguration.setProperty("hibernate.connection.password", Constrants.getDatabasePassword());
            mainConfiguration.setProperty("hibernate.connection.url", Constrants.getDatabaseUrl() + "?currentSchema=" + Constrants.getDatabaseTestSchema());
            mainConfiguration.addAnnotatedClass(Patient.class);
            mainConfiguration.addAnnotatedClass(Category.class);
            ServiceRegistry testServiceRegistry = new StandardServiceRegistryBuilder().applySettings(mainConfiguration.getProperties()).build();
            sessionFactory = mainConfiguration.buildSessionFactory(testServiceRegistry);

        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static Session getSession() throws HibernateException {
        return sessionFactory.openSession();
    }

    public static SessionFactory getSessionFactory() {
        if (null == sessionFactory) {
            buildSessionFactory();
        }
        return sessionFactory;
    }

    public static Configuration getMainConfiguration() {
        return mainConfiguration;
    }
}