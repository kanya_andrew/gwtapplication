package com.kanya.server.dao;

import com.kanya.shared.Patient;

import java.util.List;

public interface PatientDao {

    void savePatient(Patient patient);

    void updatePatientInDb(Patient patient);

    List<Patient> getAllPatientsFromDb();

    void fillOutDbWithRandomData(int numberOfPatients);

    Patient getPatient(int patientId);

}
