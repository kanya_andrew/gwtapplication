package com.kanya.server.dao.Impl;

import com.kanya.server.HibernateUtil;
import com.kanya.server.dao.PatientDao;
import com.kanya.shared.Category;
import com.kanya.shared.Patient;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PatientDaoImpl implements PatientDao {
    private static final Logger LOGGER = Logger.getLogger(PatientDaoImpl.class.toString());
    private static SessionFactory sessionFactory;

    public PatientDaoImpl(SessionFactory sessionFactory) {
        LOGGER.log(Level.INFO, "PatientDaoImpl initialized");
        LOGGER.log(Level.INFO, "sessionFactory is null" + (sessionFactory == null));
        PatientDaoImpl.sessionFactory = sessionFactory;
    }

    @Override
    public Patient getPatient(int patientId) {
        LOGGER.log(Level.INFO, "getPatient() started");
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Patient patient = session.get(Patient.class, 1);

        transaction.commit();
        session.close();
        return patient;
    }


    @Override
    public void savePatient(Patient patient) {
        LOGGER.log(Level.INFO, "savePatient() started");

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.save(patient);

        transaction.commit();
        session.close();
    }

    @Override
    public void updatePatientInDb(Patient patient) {
        LOGGER.log(Level.INFO, "updatePatientInDb() started");

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        session.update(patient);

        transaction.commit();
        LOGGER.log(Level.INFO, "updatePatientInDb() finished");
        session.close();
    }


    @Override
    public List<Patient> getAllPatientsFromDb() {
        LOGGER.log(Level.INFO, "getAllPatientsFromDb() started");
        List<Patient> patientList;

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Query query = session.createQuery(" from patient order by id");

        patientList = query.list();

        transaction.commit();
        session.close();

        LOGGER.log(Level.INFO, "getAllPatientsFromDb new patients list size = " + patientList.size());
        return patientList;
    }

    @Override
    public void fillOutDbWithRandomData(int numberOfPatients) {
        LOGGER.log(Level.INFO, "fillOutPatientsList() started");

        List<Patient> patientList = generateListOfPatients(numberOfPatients);

        Session session = sessionFactory.openSession();
        session.beginTransaction();

        for (Patient patient : patientList) {
            session.save(patient);
        }
        session.getTransaction().commit();
        session.close();

    }

    private List<Patient> generateListOfPatients(int numberOfPatients) {
        LOGGER.log(Level.INFO, "generateListOfPatients() started");

        List<Patient> patientsList = new ArrayList<>();
        for (int i = 1; i <= numberOfPatients; i++) {
            Category patientCategory = Category.FUNNY;
            Patient patient = new Patient();
            patient.setFirstName("Alena " + i);
            patient.setLastName("Bond");
            patient.setDeleted(false);
            patient.setAddress(numberOfPatients + i + " Fourth Road");
            patient.setCategory(patientCategory);
            patientsList.add(patient);
        }
        LOGGER.log(Level.INFO, "generated patientsList size: " + patientsList.size());
        return patientsList;
    }
}
