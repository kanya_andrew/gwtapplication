package com.kanya.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootPanel;
import com.kanya.client.presenter.Impl.PatientsTablePresenter;


public class Index implements EntryPoint {

    public void onModuleLoad() {

        PatientsTablePresenter patientsTablePresenter = new PatientsTablePresenter();
        RootPanel.get().add(patientsTablePresenter.getFlowPanel());

    }

}
