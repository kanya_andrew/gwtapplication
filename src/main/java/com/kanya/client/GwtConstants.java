package com.kanya.client;

import com.google.gwt.i18n.client.Constants;

public interface GwtConstants extends Constants {

    @Key("tableColumnId")
    String tableColumnId();

    @Key("tableColumnAddress")
    String tableColumnAddress();

    @Key("tableColumnFirstName")
    String tableColumnFirstName();

    @Key("tableColumnLastName")
    String tableColumnLastName();

    @Key("newCategory")
    String newCategory();

    @Key("funnyCategory")
    String funnyCategory();

    @Key("hardCategory")
    String hardCategory();
}
