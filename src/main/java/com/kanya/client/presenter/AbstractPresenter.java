package com.kanya.client.presenter;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.FlowPanel;
import com.kanya.client.GwtConstants;
import com.kanya.client.rpcService.RpcService;
import com.kanya.client.rpcService.RpcServiceAsync;

import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class AbstractPresenter implements Presenter {

    private static final Logger LOGGER = Logger.getLogger(AbstractPresenter.class.toString());
    protected static final GwtConstants GWT_CONSTANTS = GWT.create(GwtConstants.class);
    protected FlowPanel flowPanel = new FlowPanel();

    protected RpcServiceAsync serviceAsync;

    protected static final String GET_PATIENTS_SERVLET = "RpcService";
    protected static final String EDIT_PATIENT_SERVLET = "RpcService";
    protected static final String ADD_PATIENT_SERVLET = "RpcService";

    public AbstractPresenter() {
    }

    @Override
    public GwtConstants getConstants() {
        return GWT_CONSTANTS;
    }

    protected void initialiseRpcService(String url) {
        LOGGER.log(Level.INFO, "initialiseRpcService() started");

        this.serviceAsync = GWT.create(RpcService.class);
        ServiceDefTarget endpoint = (ServiceDefTarget) this.serviceAsync;
        endpoint.setServiceEntryPoint(url);
    }

    @Override
    public FlowPanel getFlowPanel() {
        return flowPanel;
    }

    @Override
    public void setFlowPanel(FlowPanel flowPanel) {
        this.flowPanel = flowPanel;
    }

}
