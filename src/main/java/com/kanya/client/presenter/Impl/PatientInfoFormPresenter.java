package com.kanya.client.presenter.Impl;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;
import com.kanya.client.GwtConstants;
import com.kanya.client.presenter.AbstractPresenter;
import com.kanya.client.view.PatientInfoForm;
import com.kanya.shared.Patient;

import java.util.logging.Level;
import java.util.logging.Logger;



public class PatientInfoFormPresenter extends AbstractPresenter {


    public interface Display {

        void setPresenter(PatientInfoFormPresenter patientInfoFormPresenter);

        Widget asWidget();

        void initialiseWidget();

        void setConstants(GwtConstants gwtConstants);
    }

    private static final Logger LOGGER = Logger.getLogger(PatientInfoFormPresenter.class.toString());

    private Display patientInfoFormView;

    PatientsTablePresenter patientsTablePresenter;

    public PatientInfoFormPresenter() {
        patientInfoFormView = new PatientInfoForm();
        LOGGER.log(Level.INFO, "patientInfoFormView was created");

        bind();
    }

    @Override
    public void bind() {
        LOGGER.log(Level.INFO, "bind() started");

        patientInfoFormView.setPresenter(this);
        patientInfoFormView.setConstants(GWT_CONSTANTS);
        patientInfoFormView.initialiseWidget();
    }

    @Override
    public void refreshDisplay() {
        patientsTablePresenter.refreshDisplay();
    }

    public void addPatientIntoDataBase(Patient patient) {
        LOGGER.log(Level.INFO, "addPatientIntoDataBase() started");
        initialiseRpcService(GWT.getModuleBaseURL() + ADD_PATIENT_SERVLET);
        this.serviceAsync.addPatient(patient, new DefaultCallback());
    }

    public void updatePatientInDb(Patient patientToEdit) {
        LOGGER.log(Level.INFO, "updatePatientInDb() started");

        initialiseRpcService(GWT.getModuleBaseURL() + EDIT_PATIENT_SERVLET);
        this.serviceAsync.editPatient(patientToEdit, new DefaultCallback());
    }

    public void setPatientInfoFormView(Display patientInfoFormView) {
        this.patientInfoFormView = patientInfoFormView;
    }

    public PatientsTablePresenter getPatientsTablePresenter() {
        return patientsTablePresenter;
    }

    public void setPatientsTablePresenter(PatientsTablePresenter patientsTablePresenter) {
        this.patientsTablePresenter = patientsTablePresenter;
    }

    public Display getPatientInfoFormView() {
        return patientInfoFormView;
    }

    public void setPatientInfoFormView(PatientInfoForm patientInfoFormView) {
        this.patientInfoFormView = patientInfoFormView;
    }


    private class DefaultCallback implements AsyncCallback {

        @Override
        public void onFailure(Throwable caught) {
            LOGGER.log(Level.INFO, "DefaultCallback.onFailure() started");
            Window.alert("Fail");
        }

        @Override
        public void onSuccess(Object result) {
            LOGGER.log(Level.INFO, "DefaultCallback.onSuccess() started");
                Window.alert("Success");
            patientsTablePresenter.refreshDisplay();

        }
    }


}
