package com.kanya.client.presenter.Impl;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.ListDataProvider;
import com.kanya.client.GwtConstants;
import com.kanya.client.presenter.AbstractPresenter;
import com.kanya.client.presenter.Presenter;
import com.kanya.client.view.PatientsTable;
import com.kanya.shared.Patient;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PatientsTablePresenter extends AbstractPresenter implements Presenter {

    public interface Display {

        void setPresenter(PatientsTablePresenter patientsTablePresenter);

        Widget asWidget();

        void setPatientsList(List<Patient> patientsList);

        void setGwtConstants(GwtConstants gwtConstants);

        void displayPatientsList();

        List<Patient> getPatientsList();

        void setPatientInfoForm(PatientInfoFormPresenter.Display patientInfoFormView);

        ListDataProvider<Patient> getDataProvider();
    }

    private static final Logger LOGGER = Logger.getLogger(PatientsTablePresenter.class.toString());

    private List<Patient> patientsList = new ArrayList<>();
    private Display patientsTableView;

    public PatientsTablePresenter() {
        patientsTableView = new PatientsTable();
        LOGGER.log(Level.INFO, "patientsTableView was created");
        fetchListOfPatients();
    }

    @Override
    public void bind() {
        LOGGER.log(Level.INFO, "bind() started");

        patientsTableView.setPresenter(this);
        patientsTableView.setPatientsList(patientsList);
        patientsTableView.setGwtConstants(GWT_CONSTANTS);
        patientsTableView.displayPatientsList();
    }


    public List<Patient> getPatientsList() {
        return patientsList;
    }

    public void setPatientsList(List<Patient> patientsList) {
        this.patientsList = patientsList;
        LOGGER.log(Level.INFO, "presenter " + String.valueOf(patientsList.size()));
    }

    public Display getPatientsTableView() {
        return patientsTableView;
    }

    public void setPatientsTableView(PatientsTable patientsTableView) {
        this.patientsTableView = patientsTableView;
    }

    private void fetchListOfPatients() {
        LOGGER.log(Level.INFO, "fetchListOfPatients() started");

        initialiseRpcService(GWT.getModuleBaseURL() + GET_PATIENTS_SERVLET);
        this.serviceAsync.getListOfPatients(new DefaultCallback());
    }

    public void fillOutDbWithRandomData(int numberOfPatients) {
        LOGGER.log(Level.INFO, "fillOutDbWithRandomData() started");

        initialiseRpcService(GWT.getModuleBaseURL() + GET_PATIENTS_SERVLET);
        this.serviceAsync.fillOutDbWithRandomData(numberOfPatients, new FilloutCallback());
    }

    private void bindInfoFormAndTable(PatientInfoFormPresenter.Display patientInfoForm, PatientsTablePresenter.Display patientsTable) {
        patientsTable.setPatientInfoForm(patientInfoForm);
    }

    @Override
    public void refreshDisplay() {
        LOGGER.log(Level.INFO, "refreshDisplay() started");
        initialiseRpcService(GWT.getModuleBaseURL() + GET_PATIENTS_SERVLET);
        LOGGER.log(Level.INFO, "UpdateCallback.onSuccess() old patients list size = " + patientsList.size()) ;
            this.serviceAsync.getListOfPatients(new UpdateCallback());
    }

    private class DefaultCallback implements AsyncCallback {

        @Override
        public void onFailure(Throwable caught) {
        }

        @Override
        public void onSuccess(Object result) {
            LOGGER.log(Level.INFO, "DefaultCallback.onSuccess() started");
            if (result instanceof List) {

                PatientInfoFormPresenter patientInfoFormPresenter = new PatientInfoFormPresenter();
                PatientInfoFormPresenter.Display patientInfoForm = patientInfoFormPresenter.getPatientInfoFormView();
                flowPanel.add(patientInfoForm.asWidget());

                List<Patient> patientsList = (List<Patient>) result;
                PatientsTablePresenter.this.patientsList = (patientsList);
                PatientsTablePresenter.this.bind();
                PatientsTablePresenter.Display patientsTable = PatientsTablePresenter.this.getPatientsTableView();

                bindInfoFormAndTable(patientInfoForm, patientsTable);

                patientInfoFormPresenter.setPatientsTablePresenter(PatientsTablePresenter.this);
                flowPanel.add(patientsTable.asWidget());
            }
        }
    }

    private class UpdateCallback implements AsyncCallback {

        @Override
        public void onFailure(Throwable caught) {
        }

        @Override
        public void onSuccess(Object result) {
            LOGGER.log(Level.INFO, "UpdateCallback.onSuccess() started");
            if (result instanceof List) {

                List<Patient> patientsList = (List<Patient>) result;
                LOGGER.log(Level.INFO, "UpdateCallback.onSuccess() new patients list size = " + patientsList.size()) ;
                PatientsTablePresenter.this.patientsList = (patientsList);
                PatientsTablePresenter.Display patientsTable = PatientsTablePresenter.this.getPatientsTableView();

                patientsTable.setPatientsList(patientsList);
                LOGGER.log(Level.INFO, "UpdateCallback.onSuccess() new patients list size in view= " + patientsTable.getPatientsList().size()) ;

                ListDataProvider<Patient> dataProvider = patientsTable.getDataProvider();
                dataProvider.setList(patientsList);
                dataProvider.refresh();
            }
        }
    }

    private class FilloutCallback implements AsyncCallback {

        @Override
        public void onFailure(Throwable caught) {
        }

        @Override
        public void onSuccess(Object result) {
            LOGGER.log(Level.INFO, "FilloutCallback.onSuccess() started");
            PatientsTablePresenter.this.refreshDisplay();
        }
    }
}

