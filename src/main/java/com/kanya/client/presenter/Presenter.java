package com.kanya.client.presenter;

import com.google.gwt.user.client.ui.FlowPanel;
import com.kanya.client.GwtConstants;

public interface Presenter {

    void bind();
    GwtConstants getConstants();
    FlowPanel getFlowPanel();
    void setFlowPanel(FlowPanel flowPanel);

    void refreshDisplay();
}
