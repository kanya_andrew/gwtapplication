package com.kanya.client.view;


import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.*;
import com.kanya.client.GwtConstants;
import com.kanya.client.presenter.Impl.PatientInfoFormPresenter;
import com.kanya.client.presenter.Presenter;
import com.kanya.shared.Category;
import com.kanya.shared.Patient;

import java.util.logging.Level;
import java.util.logging.Logger;

public class PatientInfoForm extends Composite implements PatientInfoFormPresenter.Display {

    private static final Logger LOGGER = Logger.getLogger(PatientInfoForm.class.toString());

    private PatientInfoFormPresenter patientInfoFormPresenter;
    private static Binder uiBinder = GWT.create(Binder.class);

    @UiField
    TextArea addressBox;
    @UiField
    ListBox categoryBox;
    @UiField
    Button createButton;
    @UiField
    TextBox firstNameBox;
    @UiField
    TextBox lastNameBox;
    @UiField
    Button updateButton;

    private Patient patientToEdit;
    private GwtConstants constants;

    interface Binder extends UiBinder<Widget, PatientInfoForm> {
    }

    public PatientInfoForm() {

    }

    @Override
    public void initialiseWidget() {
        LOGGER.log(Level.INFO, "initialiseWidget() started");
        initWidget(uiBinder.createAndBindUi(this));

        // Add the categories to the category box.
        for (Category category : Category.values()) {
            categoryBox.addItem(getLocaleForEmun(category));
        }

        // Initialize the patientToEdit to null.
        setPatientToEdit(null);

        // Handle events.
        updateButton.addClickHandler(new ClickHandler() {

            public void onClick(ClickEvent event) {
                LOGGER.log(Level.INFO, "updateButton.onClick() started");
                if (patientToEdit == null) {
                    return;
                }
                LOGGER.log(Level.INFO, "updateButton.onClick() patientToEdit" + patientToEdit.toString());
                // Update the patientToEdit.
                patientToEdit.setFirstName(firstNameBox.getText());
                patientToEdit.setLastName(lastNameBox.getText());
                patientToEdit.setAddress(addressBox.getText());
                int categoryIndex = categoryBox.getSelectedIndex();
                patientToEdit.setCategory(getEnumForLocaleView(categoryIndex));
                LOGGER.log(Level.INFO, "updateButton.onClick() patientToEdit before to sent" + patientToEdit.toString());
                patientInfoFormPresenter.updatePatientInDb(patientToEdit);
            }
        });
        createButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                Patient newPatient = new Patient();
                String firstName = firstNameBox.getText();
                String lastName = lastNameBox.getText();
                String address = addressBox.getText();
                newPatient.setFirstName(firstName);
                newPatient.setLastName(lastName);
                newPatient.setAddress(address);
                int categoryIndex = categoryBox.getSelectedIndex();
                newPatient.setCategory(getEnumForLocaleView(categoryIndex));
                LOGGER.log(Level.INFO, "createButton.onClick() new Patient was created" + newPatient.toString());
                patientInfoFormPresenter.addPatientIntoDataBase(newPatient);
            }
        });

    }

    @Override
    public void setConstants(GwtConstants gwtConstants) {
        this.constants = gwtConstants;
    }

    public void setPatientToEdit(Patient patientToEdit) {
        LOGGER.log(Level.INFO, "setPatientToEdit() started");
        this.patientToEdit = patientToEdit;
        updateButton.setEnabled(patientToEdit != null);
        if (patientToEdit != null) {
            firstNameBox.setText(patientToEdit.getFirstName());
            lastNameBox.setText(patientToEdit.getLastName());
            addressBox.setText(patientToEdit.getAddress());
            Category patientCategory = patientToEdit.getCategory();
            int selectedIndex = 0;
            for (Category category : Category.values()) {
                if (patientCategory == category) {
                    categoryBox.setSelectedIndex(selectedIndex);
                    break;
                }
                selectedIndex++;
            }
        }
    }

    private String getLocaleForEmun(Category element) {
        LOGGER.log(Level.INFO, "getLocaleForEmun() started");
        String categoryBoxValue;
        switch (element) {
            case NEW:
                categoryBoxValue = constants.newCategory();
                break;

            case FUNNY:
                categoryBoxValue = constants.funnyCategory();
                break;

            case HARD:
                categoryBoxValue = constants.hardCategory();
                break;

            default:
                categoryBoxValue = constants.newCategory();
                break;
        }
        return categoryBoxValue;
    }

    private Category getEnumForLocaleView(int categoryIndex) {
        LOGGER.log(Level.INFO, "getEnumForLocaleView() started");
        Category category = null;
        int counter = 0;
        for (Category currentCategory : Category.values()) {
            if (categoryIndex == counter) {
                category = currentCategory;
                break;
            }
            counter++;
        }
        return category;
    }

    @Override
    public void setPresenter(PatientInfoFormPresenter presenter) {
        this.patientInfoFormPresenter = presenter;
    }

    @Override
    public Widget asWidget() {
        return this;
    }

    public static Binder getUiBinder() {
        return uiBinder;
    }

    public static void setUiBinder(Binder uiBinder) {
        PatientInfoForm.uiBinder = uiBinder;
    }

    public TextArea getAddressBox() {
        return addressBox;
    }

    public void setAddressBox(TextArea addressBox) {
        this.addressBox = addressBox;
    }

    public ListBox getCategoryBox() {
        return categoryBox;
    }

    public void setCategoryBox(ListBox categoryBox) {
        this.categoryBox = categoryBox;
    }

    public Button getCreateButton() {
        return createButton;
    }

    public void setCreateButton(Button createButton) {
        this.createButton = createButton;
    }

    public TextBox getFirstNameBox() {
        return firstNameBox;
    }

    public void setFirstNameBox(TextBox firstNameBox) {
        this.firstNameBox = firstNameBox;
    }

    public TextBox getLastNameBox() {
        return lastNameBox;
    }

    public void setLastNameBox(TextBox lastNameBox) {
        this.lastNameBox = lastNameBox;
    }

    public Button getUpdateButton() {
        return updateButton;
    }

    public void setUpdateButton(Button updateButton) {
        this.updateButton = updateButton;
    }

    public Patient getPatientToEdit() {
        return patientToEdit;
    }

    public PatientInfoFormPresenter getPatientInfoFormPresenter() {
        return patientInfoFormPresenter;
    }

    public void setPatientInfoFormPresenter(PatientInfoFormPresenter patientInfoFormPresenter) {
        this.patientInfoFormPresenter = patientInfoFormPresenter;
    }

    public Presenter getPresenter() {
        return patientInfoFormPresenter;
    }
}