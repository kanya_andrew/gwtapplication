package com.kanya.client.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.cellview.client.SimplePager.TextLocation;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import com.kanya.client.GwtConstants;
import com.kanya.client.presenter.Impl.PatientInfoFormPresenter;
import com.kanya.client.presenter.Impl.PatientsTablePresenter;
import com.kanya.shared.Patient;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class PatientsTable extends Composite implements PatientsTablePresenter.Display {

    private static final Logger LOGGER = Logger.getLogger(PatientsTable.class.toString());

    private ListDataProvider<Patient> dataProvider = new ListDataProvider<>();
    private List<Patient> patientsList = new ArrayList<>();
    private PatientsTablePresenter patientsTablePresenter;

    interface Binder extends UiBinder<Widget, PatientsTable> {
    }

    @UiField(provided = true)
    CellTable cellTable;

    @UiField(provided = true)
    SimplePager pager;

    @UiField
    IntegerBox numberOfMocks;

    @UiField()
    Button createButton;

    PatientInfoForm patientInfoForm;

    private GwtConstants gwtConstants;

    private static PatientsTable instance;

    public PatientsTable() {

    }

    private void initTableColumns() {
        LOGGER.log(Level.INFO, "initTableColumns() started");


        // Id.
        TextColumn<Patient> idColumn = new TextColumn<Patient>() {
            @Override
            public String getValue(Patient contact) {
                return String.valueOf(contact.getId());
            }
        };
        idColumn.setSortable(true);
        cellTable.addColumn(idColumn, gwtConstants.tableColumnId());
        cellTable.setColumnWidth(idColumn, 10, Unit.PCT);
        idColumn.setSortable(true);

        // First name.
        TextColumn<Patient> firstNameColumn = new TextColumn<Patient>() {
            @Override
            public String getValue(Patient contact) {
                return contact.getFirstName();
            }
        };
        cellTable.addColumn(firstNameColumn, gwtConstants.tableColumnFirstName());
        cellTable.setColumnWidth(firstNameColumn, 20, Unit.PCT);

        // Last name.
        TextColumn<Patient> lastNameColumn = new TextColumn<Patient>() {
            @Override
            public String getValue(Patient contact) {
                return contact.getLastName();
            }
        };
        lastNameColumn.setSortable(true);
        cellTable.addColumn(lastNameColumn, gwtConstants.tableColumnLastName());
        cellTable.setColumnWidth(lastNameColumn, 20, Unit.PCT);

        // Address.
        TextColumn<Patient> addressColumn = new TextColumn<Patient>() {
            @Override
            public String getValue(Patient contact) {
                return contact.getAddress();
            }
        };
        addressColumn.setSortable(true);
        addressColumn.setDefaultSortAscending(false);
        cellTable.addColumn(addressColumn, gwtConstants.tableColumnAddress());
        cellTable.setColumnWidth(addressColumn, 50, Unit.PCT);

        // Connect the list to the data provider.
        dataProvider.addDataDisplay(cellTable);
        List<Patient> list = dataProvider.getList();
        for (Patient patient : patientsList) {
            list.add(patient);
        }

        final SingleSelectionModel<Patient> selectionModel = new SingleSelectionModel<>();
        cellTable.setSelectionModel(selectionModel);
        selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
            public void onSelectionChange(SelectionChangeEvent event) {
                patientInfoForm.setPatientToEdit(selectionModel.getSelectedObject());
            }
        });
    }

    @Override
    public Widget asWidget() {
        return this;
    }

    @Override
    public void setPresenter(PatientsTablePresenter presenter) {
        this.patientsTablePresenter = presenter;
    }

    @Override
    public void setGwtConstants(GwtConstants gwtConstants) {
        this.gwtConstants = gwtConstants;
    }

    @Override
    public void displayPatientsList() {

        // Create a PatientsTable.
        cellTable = new CellTable();

        // Do not refresh the headers and footers every time the data is updated.
        cellTable.setAutoHeaderRefreshDisabled(true);
        cellTable.setAutoFooterRefreshDisabled(true);

        cellTable.setWidth("100%");

        // Create a Pager to control the table.
        SimplePager.Resources pagerResources = GWT.create(SimplePager.Resources.class);
        pager = new SimplePager(TextLocation.CENTER, pagerResources, false, 0, true);
        pager.setDisplay(cellTable);

        // Initialize the columns.
        initTableColumns();

        // Create the UiBinder.
        Binder uiBinder = GWT.create(Binder.class);

        initWidget(uiBinder.createAndBindUi(this));

        createButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                LOGGER.log(Level.INFO, "createButton.onClick() fillOutDbWithRandomData started");
                LOGGER.log(Level.INFO, "createButton.onClick() fillOutDbWithRandomData started");
                patientsTablePresenter.fillOutDbWithRandomData(Integer.parseInt(numberOfMocks.getText()));

                // Update the views.
                patientsTablePresenter.refreshDisplay();
            }
        });

    }

    @Override
    public void setPatientInfoForm(PatientInfoFormPresenter.Display patientInfoFormView) {
        patientInfoForm = (PatientInfoForm) patientInfoFormView;
    }

    @Override
    public void setPatientsList(List<Patient> patientsList) {
        this.patientsList = patientsList;
    }

    public ListDataProvider<Patient> getDataProvider() {
        return dataProvider;
    }

    public void setDataProvider(ListDataProvider<Patient> dataProvider) {
        this.dataProvider = dataProvider;
    }

    public List<Patient> getPatientsList() {
        return patientsList;
    }


    public PatientsTablePresenter getPatientsTablePresenter() {
        return patientsTablePresenter;
    }

    public void setPatientsTablePresenter(PatientsTablePresenter patientsTablePresenter) {
        this.patientsTablePresenter = patientsTablePresenter;
    }

    public CellTable getCellTable() {
        return cellTable;
    }

    public void setCellTable(CellTable cellTable) {
        this.cellTable = cellTable;
    }

    public SimplePager getPager() {
        return pager;
    }

    public void setPager(SimplePager pager) {
        this.pager = pager;
    }

    public PatientInfoForm getPatientInfoForm() {
        return patientInfoForm;
    }

    public void setPatientInfoForm(PatientInfoForm patientInfoForm) {
        this.patientInfoForm = patientInfoForm;
    }

    public GwtConstants getGwtConstants() {
        return gwtConstants;
    }

    public static PatientsTable getInstance() {
        return instance;
    }

    public static void setInstance(PatientsTable instance) {
        PatientsTable.instance = instance;
    }


}