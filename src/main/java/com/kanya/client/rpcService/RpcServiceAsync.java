package com.kanya.client.rpcService;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.kanya.shared.Patient;

import java.util.List;

public interface RpcServiceAsync {

    void addPatient(Patient patient, AsyncCallback<Integer> callback);

    void getPatientById(int patientId, AsyncCallback<Patient> callback);

    void deletePatient(Patient patient, AsyncCallback<Integer> callback);

    void editPatient(Patient patient, AsyncCallback<Integer> callback);

    void getListOfPatients(AsyncCallback<List<Patient>> callback);

    void fillOutDbWithRandomData(int numberOfPatients, AsyncCallback<Integer> callback);
}
