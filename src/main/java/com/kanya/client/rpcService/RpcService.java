package com.kanya.client.rpcService;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.kanya.shared.Patient;

import java.util.List;

@RemoteServiceRelativePath("RpcService")
public interface RpcService extends RemoteService {

    int addPatient(Patient patient);

    Patient getPatientById(int patientId);

    List<Patient> getListOfPatients();

    int deletePatient(Patient patient);

    int editPatient(Patient patient);

    int fillOutDbWithRandomData(int numberOfPatients);
}
