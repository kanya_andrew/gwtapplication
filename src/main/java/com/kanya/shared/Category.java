package com.kanya.shared;

import java.io.Serializable;

public enum Category implements Serializable{
    NEW,
    FUNNY,
    HARD
}
